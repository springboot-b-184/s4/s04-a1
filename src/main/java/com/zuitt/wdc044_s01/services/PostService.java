package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;

public interface PostService {
    void createPost(Long userid, Post post);
    void updatePost(Long id, Post post);
    void deletePost(Long id);
    Iterable<Post> getMyPosts(Long userid);
    Iterable<Post> getPosts();

}
